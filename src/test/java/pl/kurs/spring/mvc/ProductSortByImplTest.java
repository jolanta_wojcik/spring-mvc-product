package pl.kurs.spring.mvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.kurs.spring.mvc.model.Product;
import pl.kurs.spring.mvc.sorting.ProdcutSortByImpl;
import pl.kurs.spring.mvc.sorting.ProductSortByStrategy;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=ProductSortByImplTest.ProductSortByStrategyCofigurationTest.class)
public class ProductSortByImplTest {

	@Autowired
	private ProductSortByStrategy productStrategy;

	@Test
	public void shouldSortProductsByName() {
		// given
		List<Product> products = Arrays.asList(new Product("Z", 10, "RTV"), new Product("A", 20, "AGD"));
		String criteria = "product_name";
		// when
		Collections.sort(products, productStrategy.sortBy(criteria));
		// then
		Assert.assertTrue(products.get(0).getName().equals("A"));
		Assert.assertTrue(products.get(1).getName().equals("Z"));
	}

	@Configuration
	public static class ProductSortByStrategyCofigurationTest {
		@Bean
		public ProductSortByStrategy productSortByStrategy() {
			return new ProdcutSortByImpl();
		}
	}

}
