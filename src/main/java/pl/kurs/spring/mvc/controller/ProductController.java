package pl.kurs.spring.mvc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.kurs.spring.mvc.model.Product;
import pl.kurs.spring.mvc.sorting.ProductSortByStrategy;

@Controller
@RequestMapping("/product")
public class ProductController {
	
	private List<Product> database;
	@Autowired
	private ProductSortByStrategy sortBy;

	@PostConstruct
	public void init() {
		database = new ArrayList<Product>();
		database.add(new Product("Book", 123, "science fiction"));
		database.add(new Product("Shoes", 145, "Clothes"));
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String viewAllProducts(@RequestParam(value = "sortByCriteria", defaultValue = "id") String criteria,
			ModelMap model, HttpSession session) {
		order(criteria, session);
		model.addAttribute("product", database);
		return "product";
	}

	public void orderAsc(String criteria, HttpSession session) {
		session.setAttribute("product_sort_direction", 1);
		database.sort(sortBy.sortBy(criteria));
	}

	public void orderDsc(String criteria, HttpSession session) {
		session.setAttribute("product_sort_direction", 2);
		database.sort(sortBy.sortByReverse(criteria));
	}

	public void order(String criteria, HttpSession session) {
		Integer sort = (Integer) Optional.ofNullable(session.getAttribute("product_sort_direction"))
				.orElse(new Integer(0));
		switch (sort) {
		case 0:
			orderAsc(criteria, session);
			break;
		case 1:
			orderDsc(criteria, session);
			break;
		case 2:
			orderAsc(criteria, session);
			break;
		}
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String addProduct(@ModelAttribute Product product, ModelMap model, HttpSession session) {
		database.add(product);
		return viewAllProducts("id", model, session);
	}

	@RequestMapping(value = "/remove/", method = RequestMethod.POST)
	public String removeProduct(@RequestParam("id") int id, ModelMap model, HttpSession session) {
		Product toRemove = database.stream().filter(e -> e.getId() == id).findFirst().get();
		database.remove(toRemove);
		return viewAllProducts("id", model, session);
	}
}
