package pl.kurs.spring.mvc.sorting;

import java.util.Comparator;

import pl.kurs.spring.mvc.model.Product;

public interface ProductSortByStrategy {
	Comparator<Product> sortBy(String criteria);
	Comparator<Product> sortByReverse(String criteria);
}
